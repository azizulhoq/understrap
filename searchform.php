<form method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="searchFormBox">
		<label class="screen-reader-text sr-only"><?php _x( 'Search for:', 'label' ); ?></label>
		<input type="text" value="<?php echo get_search_query(); ?>" name="s" placeholder="Search" autocomplete="off" />
		<button type="submit" class="searchsubmit"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/search-icon.svg" alt="serach-icon" /></button>
	</div>
</form>
