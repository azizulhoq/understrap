<?php
/**
 * understrap enqueue scripts
 *
 * @package understrap
 */

/**
 *  BOOMERANG DEV/PRODUCTION Switch
 *
 */
define('BOOM_DEV', true);
define('VERSION', '1');

function understrap_scripts() {
    wp_enqueue_style( 'font-open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700');

    if(BOOM_DEV) {
        //development files
        wp_enqueue_style( 'plugin', get_stylesheet_directory_uri() . '/css/plugin.css', array(), VERSION, 'all' );
        wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/css/style.css', array(), VERSION, 'all' );

        wp_enqueue_script('jquery');
    	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/script.js', array(), VERSION, true );


     //    wp_enqueue_script( 'understrap-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
    	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    	// 	wp_enqueue_script( 'comment-reply' );
    	// }

    } else {
        //production
        wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/css/style.min.css', array(), VERSION, 'all' );

        wp_enqueue_script('jquery');
        wp_enqueue_script( 'main', get_template_directory_uri() . '/js/script.min.js', array(), VERSION, true );


        // wp_enqueue_script( 'understrap-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
        //     wp_enqueue_script( 'comment-reply' );
        // }
    }
}

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );